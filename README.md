### We are hiring
Aktuell sind wir zwei Studenten die seit dem WiSe 14/15 Turniere auf den GameING Lanpartys des FSR organisieren. Bis jetzt sind wir jedes Semester gewachsen, haben mehr Spiele angeboten und mehr Spieler betreut. Um uns das Leben leichter zu machen, um eine bessere Organisation und Betreuung anbieten zu können und weil wir nicht ewig an der Uni sein werden suchen wir Studierende die uns helfen wollen. Es ist egal ob Du Jahrelange Erfahrung hast oder dies deine ersten Gehversuche wären. Solange du Lust hast kannst du alles weitere lernen. Gebraucht werden generell folgende Positionen:

- [ ] Turnierorganisation für: Lol, SC2, HS, CS:GO  
- [ ] Streamoperator für: Lol, SC2, HS, CS:GO
- [ ] Caster für: Lol, SC2, HS, CS:GO
- [ ] Generelle übergeordnete Turnierleitung und Organisation
- [ ] Grafikdesigner
- [ ] Videoschnitt und Bearbeitung (Teaser, Trailer und aftermovie)

##### Auch wenn ihr nicht alle Anforderungen einer Aufgabe erfüllen könnt oder wollt freuen wir uns über jede Unterstützung die wir bekommen.

#### Turnierorganisation:
Planung und Organisation von Turnierabläufen.(ua. Brackets) Eventuelles Anmelden des Turniers bei Riot/Valve/ESL/usw. Ansprechpartner für Spieler, Interessierte und andere Helfer für das betreffende Spiel. Sicherstellung des reibungslosen Turnierablaufs.


#### Streamoperator:
Bereitstellen von allem was fürs Stream nötig ist.(ua. PC, Spiel, Mikrofon). Operieren der Streaming Software, des Spiels sowie der Streamingplattform.

#### Shoutcaster:
Kommentieren des Spiels für den Stream.

###### Streamoperator und Shoutcaster lassen sich relativ einfach vereinen.

#### Generelle übergeordnete Turnierleitung und Organisation:
Leitung des gesamten Teams, Koordination der einzelnen Turniere, Streams und Grafiker.
Ansprechpartner für den Fachschaftsrat und Hauptverantwortlicher.

#### Grafikdesigner:
Erstellung von Stream overlays, Web assets für die Homepage
